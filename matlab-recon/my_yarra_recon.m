function my_yarra_recon(work_path, meas_file, output_path, temp_path)
% MY_YARRA_RECON  An example implementation of image reconstruction module
%       MY_YARRA_RECON(work_path, meas_file, output_path) reads measurement
%       data from meas_file located in work_path directory, runs fft for 
%       image reconstruction and writes the images as DICOM files to
%       output_path directory

    addpath('imagescn_R2008a/');     % for plotting images
    addpath('mapVBVD/');      % for reading TWIX files

    % ## Read the data from the Siemens TWIX file
    twix_obj=mapVBVD([work_path meas_file]);

    % Read the sequence parameters from the file
    baseresolution=double(twix_obj{2}.image.NCol);
    channels=double(twix_obj{2}.image.NCha);
    slices=double(twix_obj{2}.image.NPar);

    % Get the k-space data
    rawdata=twix_obj{2}.image();

    % Show raw data for channel 1 
    figure('Name', 'RawData'), imagescn(abs(squeeze(rawdata(:,1,:,:))),[],[],[],3);

    % Run 3D FFT for each channel 
    for ic=1:channels
        coilimage(:,ic,:,:) =fftshift(fftn(squeeze(rawdata(:,ic,:,:))));
    end

    % Show image data for channel 1 
    figure('Name', 'ImageData'), imagescn(abs(squeeze(coilimage(:,1,:,:))),[],[],[],3);

    % Calculate SSQ combination of all channels, obtain 1 3D dataset
    % (sum-of-squares calculation to combine images from all coils)

    ssq=zeros(size(squeeze(coilimage(:,1,:,:))));

    for sliceN=1:slices
        for ic=1:channels
            ssq(:,:,sliceN)=ssq(:,:,sliceN)+squeeze(abs(coilimage(:,ic,:,sliceN).*coilimage(:,ic,:,sliceN)));
        end
    end
    ssq=sqrt(ssq);

    % Cut out black stripes resulting from oversampling 
    result = ssq(end/4:end/4*3,:,:,:);

    % Show resulting image
    figure('Name', 'ResultingImage'), imagescn(result,[],[],[],3);

    % Write results into DICOM files using writedicom function
    for sliceN=1:slices
        dicomwrite(result(:,:,sliceN), [output_path '/example.slice' num2str(sliceN, '%03d') '.dcm']);
    end
end
